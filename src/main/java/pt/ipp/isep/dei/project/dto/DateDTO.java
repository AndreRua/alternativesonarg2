package pt.ipp.isep.dei.project.dto;

import java.util.Date;

public class DateDTO {
    Date initialDate;
    Date endDate;

    public Date getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
